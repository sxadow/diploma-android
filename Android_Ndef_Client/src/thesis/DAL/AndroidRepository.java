package thesis.DAL;

import android.app.Activity;
import android.content.*;

import thesis.Interfaces.IRepository;

public class AndroidRepository implements IRepository {


	final static private char[] hexArray = "0123456789ABCDEF".toCharArray();
	private SharedPreferences mSettings;
	
	public AndroidRepository(Activity activity)
	{
		mSettings = activity.getSharedPreferences("Data", Context.MODE_PRIVATE);
	}
	
	@Override
	public void Add(byte[] UUID, byte[] secret){
		SharedPreferences.Editor editor = mSettings.edit();
	    editor.putString("UUID", BytesToHex(UUID));
	    editor.putString("Secret", BytesToHex(secret));
	    editor.apply();

	}

	@Override
	public byte[] Get() {
		String mCounter = mSettings.getString("Secret", "");
		return HexToBytes(mCounter);
	}

	@Override
	public byte[] GetUUID()
	{
		byte[] uid = HexToBytes(mSettings.getString("UUID", ""));
		return uid;
	}
	
	public static String BytesToHex(byte[] bytes) 
	{
		char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) 
	    {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	public static byte[] HexToBytes(String s) 
	{
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) 
	    {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}

}
