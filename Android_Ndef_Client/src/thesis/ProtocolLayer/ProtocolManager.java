package thesis.ProtocolLayer;

import java.util.Arrays;

import thesis.DAL.AndroidRepository;
import thesis.Interfaces.*;

public class ProtocolManager implements IReceiver
{
	private IRepository _repository;
	private ICrypto _crypto;
	private ISender _sender;
	private ILog _log;
	
	private byte[] _uid;
	private byte[] _secret;
	
	private byte[] _pass;
	public ProtocolManager(IRepository Repository, ICrypto CryptoFunctions, ISender SendInterface, ILog Log)
	{
		this._repository = Repository;
		this._crypto = CryptoFunctions;
		this._sender = SendInterface;
		this._log = Log;
	}
	
	public void OnReceive(byte[] message)
	{
		if (message.length == 0)
		{
			throw new IllegalArgumentException();
		}
		if (message[0] >> 4 == 0)
		{
			byte[] result = new byte[1];
			byte[] arg = new byte[message.length - 1];
			System.arraycopy(message, 1, arg, 0, message.length - 1);
			
			switch(message[0])
			{
				case 0: 
					result = Identity(arg);
					_sender.Send(result);
					break;
				case 1: 
					NewPassword(arg);
					break;
				case 2: 
					result = Auth(arg);
					_sender.Send(result);
					break;
				case 3: 
					_sender.Result(true, "�������������� �������� ������");
					break;
				case 5:
					NewSecret(arg);
					break;
				/*default: 
					
					break;*/
			}
		}
		else
		{
			WasError();
		}
	}
	
	public void NewAuth()
	{
		_uid = _repository.GetUUID();
		/*if (_uid.length == 0)
		{
			_uid = _crypto.GetUUID(_crypto.GetUUID());
			byte[] secr = {0, 1, 2, 3};
			_repository.Add(_uid, secr);
		}*/
		byte[] firstMessage = new byte[17];
		for (int i = 1; i < firstMessage.length; i++)
		{
			firstMessage[i] = _uid[i-1];
		}
		
		print("³������� GUID �� ����'����", _uid);
		_sender.Send(firstMessage);
	}
	
	public void SendPIN(byte[] PIN)
	{
		byte[] r = ConCat(_repository.Get(), PIN);
		print("������, ���� ���������� �� ������", _repository.Get());
		byte[] likeOnPC = _crypto.GetHash(r);
		print("��� �� ������� �� PIN�", likeOnPC);
		byte[] notKey = ConCat(likeOnPC, _pass);
//		print(notKey);
		byte[] newkey = _crypto.GetHash(notKey);
//		print(newkey);
		
		byte[] result = new byte[33];
		result[0] = 0x2;
		
		for (int i = 0; i < newkey.length; i++)
		{
			result[i + 1] = newkey[i];
		}
		
		print("³������� ������������ ������ �� ����'����", newkey);
		_sender.Send(result);
	}
	
	public void NewToken()
	{
		_uid = _crypto.GetUUID(_crypto.GetUUID());
		
		//test
		byte[] secr = {0, 1, 2};
		_repository.Add(_uid, secr);
		
		byte[] result = new byte[_uid.length + 1];
		result[0] = 0x4;
		
		for (int i = 0; i < _uid.length; i++)
		{
			result[i + 1] = _uid[i];
		}
		
		print("��������� �� �������� GUID �� ����'����", _uid);
		_sender.Send(result);
	}
	
	private byte[] Identity(byte[] UUID)
	{
		byte[] result;
			_uid = UUID;
			_secret = _repository.Get();
			byte[] random = _crypto.Random();
			
			byte[] notkey = ConCat(_secret, random);
//			print(notkey);
			_secret = _crypto.GetHash(notkey);
//			print(_secret);
			result = new byte[17];
			result[0] = 0x1;
			
			for (int i = 0; i < random.length; i++)
			{
				result[i + 1] = random[i];
			}
			
		
		return result;
	}
	
	private void NewPassword(byte[] random)
	{
		print("��������� ��������������� ������������ � ����'�����", random);
		_pass = random;
		
	}
	
	private byte[] Auth(byte[] key)
	{
		byte[] result = { 0x3 };
		
//		print(key);
//		print(_secret);
		//if (_secret.equals(key))
		if (Arrays.equals(_secret, key))
		//if (_secret.hashCode() == key.hashCode())
			result[0] = 0x3;
		else
			result[0] = (byte) (0xF << 4 | 0x3);
		
		return result;
	}
	
	private void NewSecret(byte[] secret)
	{
		_repository.Add(_repository.GetUUID(), secret);
		_sender.Result(true, "�������� ��������� ������");
	}
	
	//only for test
	private void print(String info, byte[] msg)
	{
		StringBuilder sbl = new StringBuilder();
		sbl.append(info);
		sbl.append(" ");
		sbl.append(AndroidRepository.BytesToHex(msg));
		_log.Print(sbl.toString());
	}
	
	private byte[] ConCat(byte[] a, byte[] b)
	{
		if (a == null)
			return b;
		if (b == null)
			return a;
		byte[] r = new byte[a.length + b.length];
		System.arraycopy(a, 0, r, 0, a.length);
		System.arraycopy(b, 0, r, a.length, b.length);
		return r;
	}
	
	private void WasError()
	{
		_sender.Result(false, "������� ��� ��������������");
	}
}
