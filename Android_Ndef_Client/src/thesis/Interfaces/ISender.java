package thesis.Interfaces;

public interface ISender 
{
	public void Send(byte[] message);
	
	public void Result(boolean res, String message);
}
