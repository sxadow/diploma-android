package thesis.Interfaces;

public interface IRepository 
{
	public void Add(byte[] UUID, byte[] secret);
	
	public byte[] Get();
	
	public byte[] GetUUID();
}
