package thesis.Interfaces;

import java.util.UUID;

public interface ICrypto 
{
	public byte[] GetHash(byte[] inputData);
	
	//only for Android
	public UUID GetUUID();
	
	public UUID GetUUID(byte[] input);
	
	public byte[] GetUUID(UUID input);
	
	public byte[] Random();
}