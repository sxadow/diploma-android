package thesis.Interfaces;

public interface IReceiver 
{
	public void OnReceive(byte[] message);
	
	public void NewAuth();
	
	public void NewToken();
	
	public void SendPIN(byte[] PIN);
}
