package com.diploma.nfcapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultActivity extends Activity 
{
	public void onCreate(Bundle savedState) 
	{
		super.onCreate(savedState);
		setContentView(R.layout.results);
		
		Bundle extras = getIntent().getExtras();
        TextView mText = (TextView) findViewById(R.id.textInforez);
		if (extras != null)
 	   {
 		   String newText = extras.getString("Res");
 		   mText.setText(newText);
 		   
 		   if (!extras.getBoolean("pic"))
 		   {
 			  ImageView img= (ImageView) findViewById(R.id.resultImage);
 			  img.setImageResource(R.drawable.nook);
 		   }
 	   }
	}
	
	@Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("�����?");
        alertDialogBuilder
                .setMessage("�� ��������, �� ������ �����?")
                .setCancelable(false)
                .setPositiveButton("���",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNegativeButton("ͳ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
