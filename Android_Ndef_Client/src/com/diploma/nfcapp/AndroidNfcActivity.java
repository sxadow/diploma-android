/***************************************************************************
 * 
 * This file is part of the 'NDEF Tools for Android' project at
 * http://code.google.com/p/ndef-tools-for-android/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 ****************************************************************************/


package com.diploma.nfcapp;



import java.nio.charset.Charset;

import com.diploma.nfcapp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import thesis.CryptoLayer.*;
import thesis.DAL.*;
import thesis.ProtocolLayer.*;

import com.diploma.nfcapp.DefaultNfcReaderActivity;
/**
 * 
 * Boilerplate activity selector.
 * 
 * @author Thomas Rorvik Skjolberg
 *
 */


public class AndroidNfcActivity extends Activity implements thesis.Interfaces.ISender, thesis.Interfaces.ILog {
	private AndroidRepository rep;
	private CryptoFunctions crypto;
	private ProtocolManager protocol;
	private String newMessage;
	private EditText PIN;
	private byte[] tmp;
	private static final String TAG = AndroidNfcActivity.class.getName();
	
	public void toast(String message) {
		if (MyApplication.isDebug){
			Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
			toast.show();
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		new MyApplication();
		super.onCreate(savedInstanceState);
		
		rep = new AndroidRepository(this);
		crypto = new CryptoFunctions();
		
		setContentView(R.layout.main);	
		setTitle(R.string.app_name_description);
		PIN = (EditText) findViewById(R.id.PIN);
		
		protocol = new ProtocolManager(rep, crypto, this,this);
		
		newMessage = "";
		
		setContentView(R.layout.main);	
		setTitle(R.string.app_name_description);
		PIN = (EditText) findViewById(R.id.PIN);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null ) {
			String text = extras.getString("DataFromNfc");
			toast(text);
			tmp = AndroidRepository.HexToBytes(text);
			// added button for testing
			protocol.OnReceive(tmp);
		}else{
//			rep = new AndroidRepository(this);
//			crypto = new CryptoFunctions();
//			protocol = new ProtocolManager(rep, crypto, this,this);
//			newMessage = "";
			//test
//			protocol.NewToken();
//			protocol.NewAuth();
			try
			{
				protocol.NewAuth();
			}
			catch(Exception e)
			{
				protocol.NewToken();
			}
		}
	}

	public void reader(View view) {
		Log.d(TAG, "Show reader");

//		Intent intent = new Intent(this, DefaultNfcReaderActivity.class);
//		startActivity(intent);
//		if (tmp == null)
//		{
		if (PIN.getText().toString().length() >= 4)
		{
			String pinStr = PIN.getText().toString();
			byte[] pin = new byte[pinStr.length()];
			int p = Integer.parseInt(pinStr);
			
			for (int i = pin.length - 1; i >= 0; i--)
			{
				pin[i] = (byte) (p % 10);
				p /= 10;
			}
			
			((MyApplication )getApplication()).password = pin;
			protocol.SendPIN(pin);
		}
		else
		{
			// for testsing
			Intent intent = new Intent(this, DefaultNfcReaderActivity.class);
			startActivity(intent);
		}
		
	}

	public void beamer(View view) {
		Log.d(TAG, "Show beam writer");

//		Intent intent = new Intent(this, NdefSenderActivity.class);
//		startActivity(intent);
//		protocol.NewToken();
//		protocol.NewAuth();
	}

	public void writer(View view) {
//		Log.d(TAG, "Show beam writer");
//
//		Intent intent = new Intent(this, NdefSenderActivity.class);
//		startActivity(intent);

		
		if (tmp!=null) // we received data from PC
		{
			
		}else
		{
			protocol.NewToken();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	public void Send(byte[] message) {
		Log.d(TAG, "Send message");
		Intent intent = new Intent(this, NdefSenderActivity.class);
		String tmp = new String(AndroidRepository.BytesToHex(message));
		intent.putExtra("text", tmp);
		intent.putExtra("Logs", newMessage);
   		
		newMessage = "";

		startActivity(intent);
	}

	@Override
	public void Print(String Message)
	{
		newMessage += Message + "\n";
	}

	@Override
	public void Result(boolean res, String message) 
	{
		Intent intent = new Intent(this, ResultActivity.class);
		intent.putExtra("Res", message);
		intent.putExtra("pic", res);
   		
		startActivity(intent);
		
	}
}


