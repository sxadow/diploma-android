package com.diploma.nfcapp;

import java.nio.charset.Charset;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class NdefSenderActivity extends Activity implements CreateNdefMessageCallback,
OnNdefPushCompleteCallback {
	private NfcAdapter mAdapter;
	private TextView mText;
	private NdefMessage mMessage;
   
	public void toast(String message) {
		if (MyApplication.isDebug){
			Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
			toast.show();
		}
	}
	
    public void onCreate(Bundle savedState) {
    	   super.onCreate(savedState);
    	   String textToPush = "NDEF Push data sent from Nexus-S";
           mAdapter = NfcAdapter.getDefaultAdapter(this);
     	   Bundle extras = getIntent().getExtras();
     	   setContentView(R.layout.foreground_dispatch);
           mText = (TextView) findViewById(R.id.textInfo);
           String newText;
    	   if (extras != null)
    	   {
    		   textToPush = extras.getString("text");
    		   newText = extras.getString("Logs");
    		   mText.setText(mText.getText() + newText);
    	   }
    	   
           
//           if (mAdapter != null) {
//               mText.setText("Tap an ACR122 reader to push 'NDEF Push data sent from Nexus-S'");
//           } else {
//               mText.setText("This phone is not NFC enabled.");
//           }
           
   		toast(textToPush);
   		
   		CheckBox repeatChkBx = ( CheckBox ) findViewById( R.id.checkBox1 );
   		repeatChkBx.setOnCheckedChangeListener(new OnCheckedChangeListener()
   		{
   		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
   		    {
   		        if ( isChecked )
   		        {
   		        	mText.setVisibility(TextView.VISIBLE);
   		        }
   		        else
		        {
		        	mText.setVisibility(TextView.INVISIBLE);
		        }

   		    }
   		});
   		
           mMessage = new NdefMessage(
                   new NdefRecord[] { newTextRecord(textToPush, Locale.ENGLISH, true)});     
           mAdapter.setNdefPushMessage(mMessage, this);
           
    }
    
    public static NdefRecord newTextRecord(String text, Locale locale, boolean encodeInUtf8) {
        byte[] langBytes = locale.getLanguage().getBytes(Charset.forName("US-ASCII"));

        Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
        byte[] textBytes = text.getBytes(utfEncoding);

        int utfBit = encodeInUtf8 ? 0 : (1 << 7);
        char status = (char) (utfBit + langBytes.length);

        byte[] data = new byte[1 + langBytes.length + textBytes.length]; 
        data[0] = (byte) status;
        System.arraycopy(langBytes, 0, data, 1, langBytes.length);
        System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);

        return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], data);
        
    }
    /*
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) 
    {
    	if (keyCode == KeyEvent.KEYCODE_BACK) 
    	{
	    	android.os.Process.killProcess(android.os.Process.myPid());
    	}
    	return false;
    }*/
    
    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("�����?");
        alertDialogBuilder
                .setMessage("�� ��������, �� ������ �����?")
                .setCancelable(false)
                .setPositiveButton("���",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNegativeButton("ͳ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    
    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
     //  NdefMessage msg = new NdefMessage(NdefRecord.createMime(
         //       beamMsg.getText()  .toString(), null));

        return mMessage;

    }

	@Override
	public void onNdefPushComplete(NfcEvent event) {
		// TODO Auto-generated method stub
		
	}
} 